package config

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"
)

// LoadFromFile decodes the .toml file at path and loads its values into v, returning the meta and a possible error from the toml parser.
func LoadFromFile(path string, v any) (toml.MetaData, error) {
	if !strings.HasSuffix(path, ".toml") {
		return toml.MetaData{}, fmt.Errorf("path must refer to a .toml file")
	}
	return toml.DecodeFile(path, v)
}

// LoadFromDir lists the files at path and if exactly one .toml file is found it calls LoadFromFile with the path to that file.
func LoadFromDir(path string, v any) (toml.MetaData, error) {
	dir, err := os.Open(path)
	if err != nil {
		return toml.MetaData{}, fmt.Errorf("cannot read open path %v", path)
	}
	defer dir.Close()
	info, err := dir.Stat()
	if err != nil {
		return toml.MetaData{}, fmt.Errorf("cannot open directory at %v", path)
	}
	if !info.IsDir() {
		return toml.MetaData{}, fmt.Errorf("path %v is not a directory", path)
	}
	names, err := dir.Readdirnames(0)
	if err != nil {
		return toml.MetaData{}, fmt.Errorf("cannot read filenames from directory at %v", path)
	}
	candidates := make([]string, 0)
	for _, name := range names {
		if strings.HasSuffix(name, ".toml") {
			candidates = append(candidates, name)
		}
	}
	if len(candidates) < 1 {
		return toml.MetaData{}, fmt.Errorf("no .toml files were found in directory at %v", path)
	}
	if len(candidates) > 1 {
		return toml.MetaData{}, fmt.Errorf("more than one .toml file was found in directory at %v", path)
	}
	path = filepath.Join(path, candidates[0])
	return LoadFromFile(path, v)
}

// LoadFromApplicationPath gets the executable directory and then calls LoadFromDir with the path of that directory.
func LoadFromApplicationPath(v any) (toml.MetaData, error) {
	exe, err := os.Executable()
	if err != nil {
		return toml.MetaData{}, fmt.Errorf("cannot get the executable directory")
	}
	dir := filepath.Dir(exe)
	return LoadFromDir(dir, v)
}
